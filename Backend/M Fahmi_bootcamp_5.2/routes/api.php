<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\event_list;
use App\event;
use App\tiket;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/getevent', function(){

        $eventlist = event::get();

        return response()->json($eventlist);
    });

Route::post('/save', function(Request $request){

        DB::beginTransaction();

        try{
            
            // $this->validate($request, [
            //     'nama_event' => 'required',
            //     'date_event' => 'required',
            //     'event_place' => 'required',
            //     'harga_tiket' => 'required',
            //     'stock_tiket' => 'required'
            // ]); 
            // kalau pake validate tidak bisa bertambah datanya
            
            $usr = new event;
            $usr->nama_event = $request->input('nama_event');
            $usr->date_event = $request->input('date_event');
            $usr->event_place = $request->input('event_place');
            $usr->harga_tiket = $request->input('harga_tiket');
            $usr->stock_tiket = $request->input('stock_tiket');
            $usr->save();

            $eventlist = event::get();

            DB::commit();
            return response()->json($eventlist, 200);

        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(["message" => $e->getMessage()], 500);
        }

    });

    

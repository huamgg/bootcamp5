import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class ApiService {

  constructor(private http:Http, private router:Router) { }

   getData(){

    let headers = new Headers({});
    let options = new RequestOptions({ headers : headers });

    return this.http.get('http://localhost:8000/api/getevent', options)
    .map(result => result.json())
    .catch(error => Observable.throw(error.json().error) || "Server Error"); 
  }

  addData(nama_event:string, date_event:string, event_place:string, harga_tiket:string, stock_tiket:string){
    let array = {
      "nama_event" : nama_event,
      "date_event" : date_event,
      "event_place" : event_place,
      "harga_tiket" : harga_tiket,
      "stock_tiket" : stock_tiket,
    };

    let body = JSON.stringify(array); 
    let headers = new Headers({ "Content-Type" : "application/json" });
    let options = new RequestOptions({ headers : headers });

    return this.http.post('http://localhost:8000/api/save', body, options)
    .map(result => result.json());
  }
  
}



import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service';


@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  array : object[];

  constructor(private data:ApiService) { }

  ngOnInit() {
    this.data.getData()
    .subscribe(result => this.array = result);
  }

}

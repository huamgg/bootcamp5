import { Component, OnInit } from '@angular/core';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-event-master',
  templateUrl: './event-master.component.html',
  styleUrls: ['./event-master.component.css']
})
export class EventMasterComponent implements OnInit {

 
  nama_event:string ="";
  date_event:string = "";
  event_place:string = "";
  harga_tiket:string = ""; //harusnya dalam bentuk integer/decimal tapi disini error
  stock_tiket:string = ""; //harusnya  dalam bentuk integer/decimal tapi disini error
  array : object[];

  constructor(private data:ApiService) { }

  ngOnInit() {
    this.data.getData()
    .subscribe(result => this.array = result);
  }

  addData(){
    this.data.addData(this.nama_event, this.date_event, this.event_place, this.harga_tiket, this.stock_tiket)
      .subscribe(result => this.array = result);

    this.nama_event = "";
    this.date_event = "";
    this.event_place = "";
    this.harga_tiket = "";
    this.stock_tiket = "";
  }

}
